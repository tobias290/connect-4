import {Component, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

enum Space {
  FREE,
  YELLOW,
  RED,
}

enum Direction {
    HORIZONTAL,
    VERTICAL,
    DIAGONAL_LEFT,
    DIAGONAL_RIGHT,
}

@Component({
    selector: "app-game",
    templateUrl: "./game.page.html",
    styleUrls: ["./game.page.scss"],
})
export class GamePage implements OnInit {
    private spaces = [
        [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
        [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
        [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
        [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
        [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
        [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
        [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
    ];

    private twoPlayer: boolean;

    private playerTurn: Space = Space.YELLOW;

    private Space = Space;

    private isWinner: boolean = false;

    private displayNoSpaceInColumnMessage: boolean = false;

    public constructor(private router: Router, private route: ActivatedRoute) {
    }

    public ngOnInit() {
        this.twoPlayer = this.route.snapshot.data.twoPlayer;
    }

    /**
     * Gets the CSS classes needed for the current space.
     *
     * @param {Space} space - Current space type
     */
    public getSpaceCSS(space) {
      return space == Space.FREE ? "free" : space == Space.YELLOW ? "yellow" : "red";
    }

    /**
     * Restarts the game.
     */
    public restart() {
        this.playerTurn = Space.YELLOW;
        this.isWinner = false;

        this.spaces = [
            [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
            [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
            [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
            [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
            [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
            [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
            [Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE, Space.FREE],
        ];
    }

    /**
     * Quits the game and returns to the welcome page.
     */
    public quit() {
        this.restart();
        this.router.navigateByUrl("welcome");
    }

    /**
     * Places a player's counter.
     *
     * @param {number} column_i - Column counter is being placed in.
     */
    public placeCounter(column_i) {
        if (this.isWinner)
            return;

        let column = this.spaces[column_i];

        let hasSpace = false;
        let counterSpace;

        for (let row_i in column) {
            //@ts-ignore
            row_i = column.length - row_i - 1;

            if (column[row_i] === Space.FREE) {
                this.spaces[column_i][row_i] = this.playerTurn;
                hasSpace = true;
                counterSpace = row_i;
                break;
            }
        }

        this.displayNoSpaceInColumnMessage = !hasSpace;

        if (hasSpace) {
            this.isWinner =
                this.checkForWinner(column_i, counterSpace, Direction.HORIZONTAL) ||
                this.checkForWinner(column_i, counterSpace, Direction.VERTICAL) ||
                this.checkForWinner(column_i, counterSpace, Direction.DIAGONAL_LEFT) ||
                this.checkForWinner(column_i, counterSpace, Direction.DIAGONAL_RIGHT);
       
            if (!this.isWinner)
                this.playerTurn = this.playerTurn == Space.YELLOW ? Space.RED : Space.YELLOW;

            // If it is the computers players turn allow them to have a turn
            if (!this.twoPlayer && this.playerTurn === Space.RED)
                this.computerPlayersTurnBasic();
                // this.computerPlayersTurn();
        }
    }

    /**
     * Basic computer player bot.
     * Moves are made randomly.
     */
    private computerPlayersTurnBasic() {
        // Set timeout to make it look like the computer is thinking
        setTimeout(() => {
            let column = Math.floor(Math.random() * 7);

            let isSpace;

            while (!isSpace) {
                isSpace = this.spaces[column].some((val) => val === Space.FREE);

                if (isSpace)
                    this.placeCounter(column);
                else
                    column = Math.floor(Math.random() * 7);
            }
        }, 500);
    }

    private computerPlayersTurn() {
        // Space for implementing a more advanced computer player
    }

    /**
     * Checks to see if there is a winner in a certain direction.
     *
     * @param {number} col_i - Current column.
     * @param {number} row_i - Current row.
     * @param {Direction} direction - Direction currently being checked.
     *
     * @returns {boolean} - Returns true if a winner is found, false if not.
     */
    private checkForWinner(col_i, row_i, direction) {
        let foundWinner = false;

        let start, start_col, start_row;
        let end = direction === Direction.HORIZONTAL ? 7 : 6;

        start = 0;

        // Depending on the direction decided where the starting and column and row is
        if (direction === Direction.DIAGONAL_LEFT) {
            start_col = col_i - 5;
            start_row = row_i - 5;
        } else if (direction === Direction.DIAGONAL_RIGHT) {
            start_col = col_i + 5;
            start_row = row_i - 5;
        }

        /**
         * Gets the requried space.
         *
         * @param {number} col - Column index.
         * @param {number} row - Row index.
         *
         * @returns {Space|undefined} - Returns the required space or undefined if no space exists.
         */
        let getSpace = (col, row) => {
            try {
                return this.spaces[col][row];
            } catch (e) {
                return undefined;
            }
        };

        // Keep checking until no more spaces to check
        while (start <= end) {
            // Represents the 4 spaces being checked
            let spaces = [];

            // Get the next 4 spaces
            for (let i = 0; i < 4; i++) {
                let nextSpace;

                // Next space in the current four being checking will switch depending on the direction that the check is being made
                switch (direction) {
                    case Direction.HORIZONTAL:
                        nextSpace = getSpace(start + i, row_i);
                        break;
                    case Direction.VERTICAL:
                        nextSpace = getSpace(col_i, start + i);
                        break;
                    case Direction.DIAGONAL_LEFT:
                        nextSpace = getSpace(start_col + i, start_row + i);
                        break;
                    case Direction.DIAGONAL_RIGHT:
                        nextSpace = getSpace(start_col - i,  start_row + i);
                        break;
                }

                spaces.push(nextSpace);
            }

            // If the counters in the spaces array are the same color (value) then a player as won (if they are free or undefined then it doesn't count)
            // Also checking that they are not all free spaces
            if (spaces.every((val, i, arr) => val === arr[0] && val !== Space.FREE && val !== undefined)) {
                foundWinner = true;
                break;
            }

            start++;

            // Depending on the direction decided where the next starting and column and row is
            if (direction === Direction.DIAGONAL_LEFT) {
                start_col++;
                start_row++;
            } else if (direction === Direction.DIAGONAL_RIGHT) {
                start_col--;
                start_row++;
            }
        }

        return foundWinner;
    }
}
