import {Component} from "@angular/core";
import {Router} from '@angular/router';

@Component({
    selector: "app-welcome",
    templateUrl: "./welcome.page.html",
    styleUrls: ["./welcome.page.scss"],
})
export class WelcomePage {
    public constructor(private router: Router) {
    }

    /**
     * Loads the game.
     *
     * @param {boolean} twoPlayer - If true then the game will be played with two players otherwise the player will play the computer.
     */
    public navigateToPage(twoPlayer: boolean) {
        this.router.navigateByUrl(`game/${twoPlayer ? 'player' : 'computer'}`)
    }
}
