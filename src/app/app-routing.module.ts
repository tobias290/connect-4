import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
    {path: '', redirectTo: 'welcome', pathMatch: 'full'},
    {path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule'},
    {path: 'game/player', loadChildren: './game/game.module#GamePageModule', data : {twoPlayer : true}},
    {path: 'game/computer', loadChildren: './game/game.module#GamePageModule', data : {twoPlayer : false}},
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
